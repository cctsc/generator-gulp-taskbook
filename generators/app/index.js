var Generator = require('yeoman-generator');
var prompts = require('./templates/prompts.js');
var mkdirp = require('mkdirp');
var chalk = require('chalk');
var yosay = require('yosay');
var join = require('path').join;

module.exports = class extends Generator {
  initializing() {
    this.dependencies = ['gulp', 'gulp-load-plugins', 'autoprefixer',
                         'babel-preset-env', 'browser-sync', 'cssnano', 'gulp',
                         'gulp-babel', 'gulp-cached', 'gulp-concat', 'gulp-eslint',
                         'gulp-html-lint', 'gulp-htmlmin', 'gulp-imagemin',
                         'gulp-postcss', 'gulp-remember', 'gulp-sass',
                         'gulp-sourcemaps', 'gulp-stylelint',
                         'stylelint-config-standard', 'gulp-uglify',
                         'postcss-uncss', 'uncss', 'css-mqpacker'];

    this.options = {
      create: [],
      copyIndexHtml: false,
      copyIndexHtmlFromSrc: false,
      copyEslint: false,
      copyStylelint: false,
      extendPackageJson: false
    };
  }

  prompting() {
    this.log(yosay(
      `Welcome to the ${chalk.green('gulp-taskbook')}!`
    ));

    return this.prompt(prompts).then(answers => {
      this.answers = answers;
    });
  }

  configuring() {
    var answers = this.answers;

    for (var path in answers) {
      if (answers.hasOwnProperty(path) && !this.fs.exists(answers[path])) {
        this.options.create.push(answers[path]);
      }
    }

    if (!this.fs.exists('./index.html') && !this.fs.exists(join(answers.htmlSrc, '/index.html'))) {
      this.options.copyIndexHtml = true;
    }

    if (this.fs.exists(join(answers.htmlSrc, '/index.html'))) {
      this.options.copyIndexHtmlFromSrc = true;
    }

    if (!this.fs.exists(join('./.eslintrc.js'))) {
      this.options.copyEslint = true;
    }

    if (!this.fs.exists(join('./.stylelintrc.js'))) {
      this.options.copyStylelint = true;
    }

    if (this.fs.exists(join('./package.json'))) {
      this.options.extendPackageJson = true;
    }
  }

  writing() {
    var options = this.options;

    options.create.forEach((elem) => {
      mkdirp.sync(elem);
    });

    if (options.copyIndexHtml) {
      this.fs.copy(
        this.templatePath('index.html'),
        this.destinationPath(join(this.answers.htmlDest, '/index.html'))
      );

      this.fs.copy(
        this.templatePath('index.html'),
        this.destinationPath(join(this.answers.htmlSrc, '/index.html'))
      );
    }

    if (options.copyIndexHtmlFromSrc) {
      this.fs.copy(
        join(this.answers.htmlSrc, '/index.html'),
        this.destinationPath('index.html')
      );
    }

    this.fs.copyTpl(
      this.templatePath('gulpfile.js'),
      this.destinationPath('gulpfile.js'),
      this.answers
    );

    if (options.copyEslint) {
      this.fs.copy(
        this.templatePath('.eslintrc.js'),
        this.destinationPath('.eslintrc.js')
      );
    }

    if (options.copyStylelint) {
      this.fs.copy(
        this.templatePath('.stylelintrc.js'),
        this.destinationPath('.stylelintrc.js')
      );
    }

    if (options.extendPackageJson) {
      this.fs.extendJSON(
        this.destinationPath('package.json'),
        {
          scripts: {
            dev: 'gulp watch',
            html: 'gulp html',
            css: 'gulp css',
            js: 'gulp js',
            img: 'gulp image',
            lint: 'gulp jsLint cssLint htmlLint'
          }
        }
      );
      this.log(chalk.green(`Adding useful scripts to a ${chalk.white('package.json')} file.`));
      this.log(chalk.green(`Run ${chalk.bold('dev')} to watch your files and compile them when they change. Run ${chalk.bold('lint')} to lint all of your files.`));
    }
  }

  install() {
    this.yarnInstall(this.dependencies, {dev: true});
    this.installDependencies({
      yarn: true,
      npm: true,
      bower: false
    });
  }

  end() {
    this.log(chalk.green('... We are done!'));
    this.log(chalk.green(`Thanks for using ${chalk.bold('gulp-taskbook')}. Hava a nice day!`));
  }
};
