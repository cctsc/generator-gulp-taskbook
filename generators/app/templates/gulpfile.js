var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')();

var autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    mqPacker = require('css-mqpacker'),
    uncss = require('postcss-uncss');

var browserSync = require('browser-sync').create();

gulp.task('watch', () => {
  browserSync.init({
    server: {
      baseDir: '<%= htmlDest %>',
      index: 'index.html'
    }
  });

  gulp.watch('<%= jsSrc + "/**/*.js" %>', ['js']);
  gulp.watch('<%= scssSrc + "/**/*.scss" %>', ['css']);
  gulp.watch('<%= imgSrc + "/**/*" %>', ['image']);
  gulp.watch('<%= htmlSrc + "/**/*.html" %>', ['reloadHtml']);
});

gulp.task('js', ['jsLint'], () => {
  return gulp.src(['<%= jsSrc + "/**/*.js" %>', '!/node_modules/**'])
             .pipe(plugins.cached('js'))
             .pipe(plugins.sourcemaps.init({loadMaps: true}))
             .pipe(plugins.babel({
               presets: ['env']
             }))
             .pipe(plugins.remember('js'))
             .pipe(plugins.concat('bundle.js'))
             .pipe(plugins.uglify())
             .pipe(plugins.sourcemaps.write('../maps'))
             .pipe(gulp.dest('<%= jsDest %>'))
             .pipe(browserSync.stream());
});

gulp.task('jsLint', () => {
  return gulp.src(['<%= jsSrc + "/**/*.js" %>', '!/node_modules/**'])
             .pipe(plugins.cached('jsLint'))
             .pipe(plugins.eslint({fix: true}))
             .pipe(plugins.eslint.formatEach('stylish'))
             .pipe(plugins.eslint.failAfterError());
});

gulp.task('css', ['cssLint', 'html'], () => {
  return gulp.src('<%= scssSrc + "/**/*.scss" %>')
             .pipe(plugins.cached('css'))
             .pipe(plugins.sourcemaps.init({loadMaps: true}))
             .pipe(plugins.sass().on('error', plugins.sass.logError))
             .pipe(plugins.postcss([uncss({
               html: ['*.html'],
               ignore: [/.js-[\w-_]*/g]
             }), mqPacker(), autoprefixer(), cssnano()]))
             .pipe(plugins.sourcemaps.write('../maps'))
             .pipe(gulp.dest('<%= cssDest %>'))
             .pipe(browserSync.stream());
});

gulp.task('cssLint', () => {
  return gulp.src('<%= scssSrc + "/**/*.scss" %>')
             .pipe(plugins.cached('cssLint'))
             .pipe(plugins.stylelint({
               failAfterError: true,
               fix: true,
               reporters: [
                 {formatter: 'verbose', console: true}
               ],
               syntax: 'scss'
             }));
});

gulp.task('image', () => {
  return gulp.src('<%= imgSrc + "/**/*" %>')
             .pipe(plugins.cached('image'))
             .pipe(plugins.imagemin())
             .pipe(gulp.dest('<%= imgDest %>'))
             .pipe(browserSync.stream());
});

gulp.task('html', ['htmlLint'], () => {
  return gulp.src('<%= htmlSrc + "/**/*.html" %>')
             .pipe(plugins.cached('html'))
             .pipe(plugins.htmlmin())
             .pipe(gulp.dest('<%= htmlDest %>'));
});

gulp.task('htmlLint', () => {
  return gulp.src('<%= htmlSrc + "/**/*.html" %>')
             .pipe(plugins.cached('htmlLint'))
             .pipe(plugins.htmlLint())
             .pipe(plugins.htmlLint.format())
             .pipe(plugins.htmlLint.failAfterError())
});

gulp.task('reloadHtml', ['html'], (done) => {
  browserSync.reload();
  done();
});
