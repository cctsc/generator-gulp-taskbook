'use strict';

module.exports = [
  {
    type: 'input',
    name: 'htmlSrc',
    message: 'What is the location of your HTML source files?',
    default: 'src/html'
  },
  {
    type: 'input',
    name: 'htmlDest',
    message: 'Where do you want to output HTML files?',
    default: '.'
  },
  {
    type: 'input',
    name: 'scssSrc',
    message: 'What is the location of your SCSS source files?',
    default: 'src/scss'
  },
  {
    type: 'input',
    name: 'cssDest',
    message: 'Where do you want to output CSS files?',
    default: 'dist/css'
  },
  {
    type: 'input',
    name: 'jsSrc',
    message: 'What is the location of your JS source files?',
    default: 'src/js'
  },
  {
    type: 'input',
    name: 'jsDest',
    message: 'Where do you want to output JS files?',
    default: 'dist/js'
  },
  {
    type: 'input',
    name: 'imgSrc',
    message: 'What is the location of your images?',
    default: 'src/img'
  },
  {
    type: 'input',
    name: 'imgDest',
    message: 'Where do you want to output images?',
    default: 'dist/img'
  }
];
