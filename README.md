# generator-gulp-taskbook [Deprecated]

> generator-gulp-taskbook has been deprecated.

> A [Yeoman](http://yeoman.io/) generator for installing and configuring [Gulp](http://gulpjs.com/).

Gulp-taskbook installs Gulp and a few useful plugins, so you can quickly start your new project.

## Installation

Make sure you have [Node](https://nodejs.org/en/) and [npm](https://www.npmjs.com/) installed.
Then install [Yeoman](http://yeoman.io/) and generator:
```bash
npm install -g yo
npm install -g generator-gulp-taskbook
```

Create a new directory for your project:
```bash
mkdir my-new-project
cd my-new-project
``` 

Now you can install and configure Gulp with:
```bash
yo gulp-taskbook
```

## Usage and tasks
### Warning
Make sure that you know how [UnCss](https://www.npmjs.com/package/uncss) 
and [MQPacker](https://www.npmjs.com/package/css-mqpacker) plugins work. 
Without proper consideration they may break your CSS. Remember that you can 
easily remove them from gulpfile.js, if you don't need them or you aren't sure 
how to use them.

### All Gulp tasks
* watch - Task to start Browsersync, watch files and run other tasks when file change.
* js - Task to handle transpilation (with babel), concatenation and minification.
* jsLint - Task to lint javascript with ESLint.
* css - Task to compile SCSS files.
* cssLint - Task to lint SCSS files with stylelint.
* image - Task to handle images optimization.
* html - HTML minification.
* htmlLint - Task to lint HTML with htmllint.
* reloadHtml - Task to force Browsersync to reload the page.

### Tools used in the tasks
* [Browsersync](https://www.npmjs.com/package/browser-sync);
* [Postcss](https://www.npmjs.com/package/postcss) ([autoprefixer](https://www.npmjs.com/package/autoprefixer), 
[cssnano](https://www.npmjs.com/package/cssnano), [MQPacker](https://www.npmjs.com/package/css-mqpacker),
[UnCss](https://www.npmjs.com/package/uncss));
* [Babel](https://www.npmjs.com/package/gulp-babel) (with [babel-preset-env](https://github.com/babel/babel-preset-env));
* [ESLint](https://www.npmjs.com/package/gulp-eslint);
* [HTMLMinifier](https://www.npmjs.com/package/gulp-htmlmin) and [HTMLLint](https://www.npmjs.com/package/gulp-html-lint);
* [Cached](https://www.npmjs.com/package/gulp-cached), [Remember](https://www.npmjs.com/package/gulp-remember), [Uglify](https://www.npmjs.com/package/gulp-uglify), [Sourcemaps](https://www.npmjs.com/package/gulp-sourcemaps) and [Concat](https://www.npmjs.com/package/gulp-concat);
* [StyleLint](https://www.npmjs.com/package/gulp-stylelint) with [stylelint-config-standard](https://www.npmjs.com/package/stylelint-config-standard);
* [imagemin](https://www.npmjs.com/package/gulp-imagemin);
* [Sass](https://www.npmjs.com/package/gulp-sass);

## Code quality
You can check code with ESLint:
```bash
npm run eslint
```
## License
MIT © [Paweł Halczuk](https://bitbucket.org/cctsc/)
